#!/usr/bin/env bash

git clone https://aur.archlinux.org/auracle-git.git /tmp/aur-helper/auracle-git --depth 1
cd /tmp/aur-helper/auracle-git
makepkg --syncdeps --install --noconfirm
git clone https://aur.archlinux.org/pacaur.git /tmp/aur-helper/pacaur --depth 1
cd /tmp/aur-helper/pacaur
makepkg --syncdeps --install --noconfirm

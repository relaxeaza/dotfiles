#!/usr/bin/env bash

device=$1
event=$2

case $event in
    up|down)
        /usr/bin/sudo -u relax bash /home/relax/.fvwm/scripts/network.sh -- --update-label --notify --set-env
    ;;
esac;

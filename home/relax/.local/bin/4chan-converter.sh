#!/usr/bin/env bash

export icon=/tmp/4chan-converter.png

if ! [[ -f $icon ]]; then
    base64 --decode > $icon <<< "iVBORw0KGgoAAAANSUhEUgAAAFMAAABkBAMAAAAI4xEmAAAALVBMVEUABABKKjURTEt0MyqXNEbLOTmDWEMagTXkSjXtdjNno1XWsJT/vnn/6b3//9ZbBSfMAAAAAXRSTlMAQObYZgAAAe1JREFUWMPN1r1Lw0AYBvC0DiKC+KLUQRHN0lkPBD9wii7iGHBUlAMX0anQvZDR9QYFdRA6KX4sAcHNobg7dPIDRLi/wefea0yt2slc81CSo/215H3vLqnn/UjRr7Ti+/6U1y3/TllV62059XNI+wSKKFc66Tlqc0XjKzFDlTRVl7ToT1NsQmPWscLUlitkIgQN54SipS2K2hbqqIgB/Ik5jwj8hhN6p/WzEKJ+hgNT/oYpgZK2Zk4/4lspt4WUmOUEXlJuaL+ZyQMSy1prAzupXZv7WdEiVg/vQLGIa5UyoTjwnCKzuD3Y9ZcB5b3PzcJ7c1YC4LireNS2qntNURYnpVummw0TnEvSDZ3mvW7oNtpLE42mCWb1NnvK+IuKeXWs1RHLplL6XeGTHFGkQMmCW2o8ttNnkW7WLKkJ6AzhhaY+KeSoibm9J8eUA9e6dPQpl9S2tyR3gu+bz3NK5Z5+CaIoUgrb0TUt4EJx434NI5NgVRLlmI7HoFwW6FospVOK5f9onrHXAYdGg/RviFN6Y5sV1UJ+vueQFgilrDcbD1EaWx65oYMxNzTc6KQ/m5sNLZA2ncJ0roTW10LAtfi3snpMWU/qjsg/NkI21PMGzJPl0I4x6nZ7y4YOaazmCzvGKOdUv8lNO8bIBf0EE9QHjOa3nywAAAAASUVORK5CYII="
fi

function encode_video_progress () {
    while [ ! -f /tmp/vstats ];do
        sleep 0.25;
    done

    while read data; do
        encoded_frames=$(awk '{print $6}' <<< $data)
        progress=$(( 100 * $encoded_frames / $input_frames ))

        echo $progress

        if [[ $progress -gt 99 ]]; then
            return
        fi
    done < <(tail -f /tmp/vstats) | yad --progress --auto-kill --auto-close --hide-text --title '4chan Converter' --text '4chan Converter' --no-buttons --window-icon $icon --image $icon
}

function encode_video () {
    if [[ $video_downscale ]]; then
        video_downscale_cmd="-vf scale='min($video_downscale_to,iw)':-2"
    fi

    if [[ $video_disable_audio == 'yes' ]]; then
        video_disable_audio_cmd="-an"
    fi

    output="$output_dir/converted-$input_file_name.webm"

    input_fps=$(ffprobe "$input" 2>&1 | sed -n 's/.*, \(.*\) tbr.*/\1/p')
    input_duration=$(ffprobe "$input" 2>&1 | sed -n 's/.* Duration: \([^,]*\), .*/\1/p')
    input_hrs=$(cut -d ':' -f 1 <<< $input_duration)
    input_min=$(cut -d ':' -f 2 <<< $input_duration)
    input_sec=$(cut -d ':' -f 3 <<< $input_duration)
    input_frames=$(echo "($input_hrs * 3600 + $input_min * 60 + $input_sec) * $input_fps" | bc | cut -d '.' -f 1)

    nice -n 15 ffmpeg -vstats_file /tmp/vstats -i "$input" $video_downscale_cmd $video_disable_audio_cmd -fs $video_size_limit -c:v libvpx -crf 15 -minrate 500K -maxrate 2M -b:v 500K "$output" -y 2>/dev/null &
    encode_video_progress

    rm -f /tmp/vstats
    xclip -in -selection clipboard <<< $output
    notify-send "4chan Converter" "$output" -i $icon
}

function encode_image () {
    input_dimensions=$(identify $input | cut -d ' ' -f 3)
    input_width=$(cut -d 'x' -f 1 <<< $input_dimensions)
    input_height=$(cut -d 'x' -f 2 <<< $input_dimensions)

    if [[ $image_downscale == 'yes' ]]; then
        if [[ $input_width -gt $input_height ]]; then
            image_downscale_cmd="-vf scale='min($image_downscale_to,iw)':-1"
        else
            image_downscale_cmd="-vf scale=-1:'min($image_downscale_to,iw)'"
        fi
    fi

    if [[ $input_extension == 'png' ]]; then
        output="$output_dir/converted-$input_file_name.png"
        copy_mime="image/png"
    else
        output="$output_dir/converted-$input_file_name.jpg"
        copy_mime="image/jpg"
    fi

    ffmpeg -i "$input" $image_downscale_cmd -compression_level 100 -q 0 "$output" -y -v 0 &&
    notify-send "4chan Converter" "$output\n\nImage copied to clipboard" -i $icon

    xclip "$output" -selection clipboard -t $copy_mime
}

function handler () {
    video_downscale=yes
    video_downscale_to=640
    video_disable_audio=yes
    video_size_limit=2.8M
    image_downscale=yes
    image_downscale_to=1000
    output_same_dir=no

    config_file=${XDG_CONFIG_HOME:-$HOME/.config}/4chan-converter.conf
    
    if [[ -f $config_file ]]; then
        source $config_file
    fi

    input=${1##file://}
    input_file_name=$(basename -s ".${1##*.}" "$input")
    mime_type=$(file --brief --mime-type "$input")
    file_type=$(cut -d '/' -f 1 <<< $mime_type)
    input_extension=$(cut -d '/' -f 2 <<< $mime_type)
    input_location=$(dirname "$input")

    if [[ $output_same_dir == 'yes' ]]; then
        output_dir="$input_location"
    else
        output_dir="$HOME"
    fi

    if [[ $file_type == 'video' ]]; then
        encode_video
    elif [[ $file_type == 'image' ]]; then
        encode_image
    else
        notify-send "4chan Converter" "Only videos and images are supported" -i $icon
        return
    fi
}

export -f handler
export -f encode_video
export -f encode_video_progress
export -f encode_image

yad --on-top --no-buttons --window-icon $icon --image $icon --inc 2 --title '4chan Converter' --dnd --exit-on-drop 1 --command="bash -c 'handler \"%s\"'"

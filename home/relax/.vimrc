set nocompatible
filetype off

syntax enable
set clipboard=unnamedplus
set history=500
set nobackup
set nowb
set noswapfile
set expandtab
set smarttab
set autoindent
set shiftwidth=4
set tabstop=4
set scrolloff=10
set number              " show line numbers
set cursorline          " highlight current line
set wildmenu            " visual autocomplete for command menu
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
set laststatus=2

let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn)|node_modules$'
\ }
let g:ctrlp_abbrev = {
    \ 'abbrevs': [{
        \ 'pattern': ' ',
        \ 'expanded': '',
        \ 'mode': 'fprz',
    \ }]
\}


map <F2> :NERDTreeToggle<CR>

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'valloric/youcompleteme'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'terryma/vim-multiple-cursors'

call vundle#end()            " required
filetype plugin indent on    " required

nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>

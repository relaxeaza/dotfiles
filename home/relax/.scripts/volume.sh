#!/usr/bin/env bash

while [[ "$#" -gt 0 ]]; do case $1 in
    --update-label) update_label_flag=1 ;;
    --gen-devices-menu) gen_devices_menu=1 ;;
    --select-device) select_device=$2; shift ;;
    --notify) show_notif=1 ;;
    --set) change_volume=$2; shift ;;
    --mute) mute_yes=1 ;;
    --unmute) mute_no=1 ;;
    --toggle) mute_toggle=1 ;;
    --daemon) start_daemon=1 ;;
    *) value=$1 ;;
esac; shift; done

device_types=('sink' 'sink-input' 'source-output')

function gen_default_device () {
    echo "sink=$(pacmd list-sinks | grep '* index:' | cut -d ' ' -f5)"
}

function get_selected_device () {
    if ! [[ -f /tmp/fvwm-volume-device ]]; then
        gen_default_device > /tmp/fvwm-volume-device
    fi

    selected_device=$(< /tmp/fvwm-volume-device)
    selected_device_type=$(cut -d '=' -f1 <<< $selected_device)
    selected_device_id=$(cut -d '=' -f2 <<< $selected_device)
}

function valid_device () {
    until pacmd list-sinks | grep '* index:' >/dev/null; do
        sleep 0.5
    done

    if ! [[ -f /tmp/fvwm-volume-device ]]; then
        gen_default_device > /tmp/fvwm-volume-device

        return 1
    fi

    local device=$(< /tmp/fvwm-volume-device)

    if ! [[ $device =~ ^[a-z-]+=[[:digit:]]+$ ]]; then
        gen_default_device > /tmp/fvwm-volume-device

        return 1
    fi

    local type=$(cut -d '=' -f1 <<< $device)
    local id=$(cut -d '=' -f2 <<< $device)

    if pacmd list-${type}s | grep "index: $id" >/dev/null; then
        return 0
    fi

    gen_default_device > /tmp/fvwm-volume-device

    return 1
}

function get_volume () {
    device_info="$1"
    volume=$(grep 'volume: front-left' <<< $device_info | awk '{print $5}')
    volume=${volume%\%}
    echo "$volume"
}

function get_muted () {
    device_info="$1"
    grep muted <<< $device_info | cut -d ' ' -f2
}

function get_device_info () {
    pacmd list-${selected_device_type}s | grep -A11 "index: $selected_device_id"
}

function notify () {
    device_info=$(get_device_info)
    muted=$(get_muted "$device_info")
    volume=$(get_volume "$device_info")

    if [[ $muted == yes ]]; then
        icon=notification-audio-volume-muted
    elif [[ $volume -eq 0 ]]; then
        icon=notification-audio-volume-muted
    elif [[ $volume -lt 40 ]]; then
        icon=notification-audio-volume-low
    elif [[ $volume -lt 70 ]]; then
        icon=notification-audio-volume-medium
    else
        icon=notification-audio-volume-high
    fi

    label="Volume $(printf %02g $volume)%"

    if [[ $muted == yes ]]; then
        label="$label (muted)"
    fi

    notify-send --app-name volume "$label" --hint "int:value:$volume" --hint "string:x-canonical-private-synchronous:volume" --icon "$icon" --urgency low --expire-time 2000
}

function update_label () {
    get_selected_device

    device_info=$(get_device_info)
    muted=$(get_muted "$device_info")
    volume=$(get_volume "$device_info")

    if [[ $muted == yes ]]; then
        label="Mute"
        icon=volume-mute
    else
        label="$(printf %02g $volume)%"

        if [[ $volume -eq 0 ]]; then
            icon=volume-mute
        elif [[ $volume -lt 50 ]]; then
            icon=volume-low
        else
            icon=volume-high
        fi
    fi

    if [[ $last_volume_label == $label ]]; then
        return
    fi

    FvwmCommand "SendToModule Panel-Volume ChangeButton label Title '$label'"
    FvwmCommand "SendToModule Panel-Volume ChangeButton label Icon 'icons/panel/$icon.png'"

    # last_volume_label=$label
}

if [[ $select_device ]]; then
    for device_type in ${device_types[@]}; do
        if [[ $select_device == $device_type ]]; then
            valid=1

            break
        fi
    done

    if ! [[ $value =~ ^[[:digit:]]+$ ]]; then
        echo "Device id \"$id\" is invalid." >&2

        exit 1
    fi

    if ! [ $valid ]; then
        echo "Device type \"$select_device\" is invalid." >&2

        exit 1
    fi

    echo "$select_device=$value" > /tmp/fvwm-volume-device
    update_label
    kill -USR1 $(< /tmp/fvwm-volume-pid) >/dev/null

    exit 0
fi

if [[ $change_volume ]]; then
    get_selected_device
    pactl set-$selected_device_type-volume $selected_device_id "$change_volume"
    update_label

    if [[ $show_notif ]]; then
        notify
    fi
    
    exit 0
fi

if [[ $mute_yes ]]; then
    get_selected_device
    pactl set-$selected_device_type-mute $selected_device_id true
    update_label

    if [[ $show_notif ]]; then
        notify
    fi
    
    exit 0
fi

if [[ $mute_no ]]; then
    get_selected_device
    pactl set-$selected_device_type-mute $selected_device_id false

    if [[ $show_notif ]]; then
        notify
    fi

    exit 0
fi

if [[ $mute_toggle ]]; then
    get_selected_device
    pactl set-$selected_device_type-mute $selected_device_id toggle

    if [[ $show_notif ]]; then
        notify
    fi

    exit 0
fi

if [[ $update_label_flag ]]; then
    update_label

    exit 0
fi

if [[ $gen_devices_menu ]]; then
    get_selected_device

    echo 'AddToMenu VolumeDeviceMenu "Volume devices" Title Top'

    script_path=$(realpath $0)

    for device_type in ${device_types[@]}; do
        while read data; do
            device_id=$(cut -d $'\t' -f2 <<< $data)
            media_name=$(cut -d $'\t' -f3 <<< $data)
            label=$(cut -d $'\t' -f4 <<< $data)

            if [[ $media_name == 'Peak detect' ]]; then
                continue
            fi

            volume=$(ponymix get-volume --$device_type --device $device_id)

            if [[ $selected_device_type == $device_type && $selected_device_id == $device_id ]]; then
                echo "AddToMenu VolumeDeviceMenu '$label: $volume%%%icons/panel/active.png%' Exec $script_path --select-device $device_type '$device_id'"
            else
                echo "AddToMenu VolumeDeviceMenu '$label: $volume%' Exec $script_path --select-device $device_type '$device_id'"
            fi
        done < <(ponymix list --$device_type --short);
    done

    exit 0
fi

if [[ $start_daemon ]]; then
    echo $$ > /tmp/fvwm-volume-pid

    function daemon () {
        trap 'return 1' SIGUSR1

        valid_device
        get_selected_device

        local remove_event="Event 'remove' on $selected_device_type #$selected_device_id"
        local change_event="Event 'change' on $selected_device_type #$selected_device_id"

        while read data; do
            case $data in
                $remove_event) return 1 ;;
                $change_event) update_label ;;
            esac
        done < <(pactl subscribe --fake '.fvwm/bin')

        return 1
    }

    until daemon; do
        sleep 0.1
    done
fi

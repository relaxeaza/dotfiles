#!/usr/bin/env bash

trap "exit 0" TERM
export TOP_PID=$$

while [[ "$#" -gt 0 ]]; do case $1 in
    --update) update=1 ;;
    --notify) notify=1 ;;
    --list) list=1 ;;
esac; shift; done

if [[ $update ]]; then
    updatedb \
        --require-visibility 0 \
        --database-root $HOME/ \
        --add-prunepaths $HOME/.cache \
        --add-prunepaths $HOME/.config/sublime-text-3/Cache \
        --add-prunepaths $HOME/Dropbox/Documents/dotfiles \
        --add-prunenames ".git .bzr .hg .svn .cache node_modules" \
        --output ~/.local/share/mlocate.db

    if [[ $notify ]]; then
        notify-send 'Files' 'Files database updated.' -t 2000
    fi

    kill -s TERM $TOP_PID
fi

if [[ $list ]]; then
    locate --database ~/.local/share/mlocate.db '' | rofi -columns 1 -dmenu -i -p 'open' | xargs --null xdg-open
fi

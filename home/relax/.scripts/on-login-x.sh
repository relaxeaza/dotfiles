#!/usr/bin/env bash

xmodmap $HOME/.Xmodmap
xinput set-prop 'Logitech Wireless Mouse' 'libinput Middle Emulation Enabled' 1

#!/usr/bin/env bash

while true; do
    pacman -Sy >/dev/null
    pkgs=$(pacman -Qu | wc -l)
    [ $pkgs -gt 0 ] && sudo -u relax DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus notify-send "$pkgs updates available";
    sleep $((60 * 6))
done

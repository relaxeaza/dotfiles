#!/usr/bin/env bash

current=$(gsettings get org.gnome.desktop.interface gtk-theme)

if [ $current = "'Adwaita'" ]; then
    gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"
else
    gsettings set org.gnome.desktop.interface gtk-theme "Adwaita"
fi

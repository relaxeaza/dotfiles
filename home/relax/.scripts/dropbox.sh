#!/usr/bin/env bash

while [[ "$#" -gt 0 ]]; do case $1 in
    --start) start=1 ;;
    --stop) stop=1 ;;
    --toggle) toggle=1 ;;
    --sync) sync=1 ;;
    --notify) show_notif=1 ;;
esac; shift; done

if ! test -x /usr/bin/dropbox-cli; then
    notify-send 'Dropbox' 'dropbox-cli not installed.' -t 2000 -i dropbox
    exit 1
fi

function notify () {
    notify-send 'Dropbox' "$1" -t 2000 -i dropbox
}

function start () {
    if pidof dropbox >/dev/null; then
        if [[ $show_notif ]]; then
            if ! [[ $sync ]]; then
                notify 'Already running.'
            fi
        fi
    else
        dropbox-cli start &>/dev/null &

        if [[ $sync ]]; then
            notify 'Syncing...'
        else
            notify 'Running.'
        fi
    fi
}

function stop () {
    if pidof dropbox; then
        dropbox-cli stop &>/dev/null &

        if [[ $show_notif ]]; then
            if ! [[ $sync ]]; then
                notify 'Stopped.'
            fi
        fi
    else
        if [[ $show_notif ]]; then
            if ! [[ $sync ]]; then
                notify 'Not running.'
            fi
        fi
    fi
}

if [[ $toggle ]]; then
    if pidof dropbox; then
        stop
    else
        start
    fi

    exit 0
fi

if [[ $start ]]; then
    start

    exit 0
fi

if [[ $stop ]]; then
    stop

    exit 0
fi

if [[ $sync ]]; then
    if pidof dropbox; then
        exit 0
    fi

    start

    while true; do
        status=$(dropbox-cli status | head -1)

        echo "$status"

        if [[ "$status" == "Up to date" ]]; then
            break;
        fi

        sleep 3;
    done

    stop

    exit 0
fi

for i in /home/lain/.fvwm/images/icons/panel/light-flat/*.png; do
    filename=$(basename $i)
    convert $i \( +clone -background '#000' -shadow 0x0-1-1 \) +swap -background none  -layers merge /tmp/lol/$filename;
done

for i in /tmp/lol/*.png; do
    filename=$(basename $i)
    convert $i \( +clone -background '#000' -shadow 30x0+1+1 \) +swap -background none  -layers merge /tmp/lol/$filename;
done

cp /tmp/lol/*.png /home/lain/.fvwm/images/icons/panel/light-shadow/

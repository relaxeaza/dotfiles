#!/usr/bin/env bash

dotfiles=$HOME/repos/dotfiles/

rsync --dry-run --archive --times --human-readable --progress --verbose --delete --filter "merge $HOME/.scripts/backups/dotfiles.txt" / $dotfiles
echo
read -p ":: Proceed with backup? [Y/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
    rsync --archive --times --human-readable --progress --verbose --delete --filter "merge $HOME/.scripts/backups/dotfiles.txt" / $dotfiles

    cd $dotfiles
    git add --all
    git commit -m "update $(date '+%H:%M:%S %d/%m/%Y')"
    git push
fi

#!/usr/bin/env bash

file='/sys/class/leds/tpacpi::thinklight/brightness'

if [ $(cat $file) = '0' ]; then
    echo 1 > $file
else
    echo 0 > $file
fi

#!/usr/bin/env bash

while [[ "$#" -gt 0 ]]; do case $1 in
    --src) src=$2; shift ;;
    --output) output=$2; shift ;;
    *) ;;
esac; shift; done


files=$(find $src -type l,f -name '*.jpg' -o -name '*.png')
total=$(wc -l <<< $files)
index=0

for img in $files; do
    index=$((index + 1))

    ext="${img##*.}"
    name=$(basename -s ".$ext" "$img")

    if [ -f "$output/$name.webp" ]; then
        continue
    fi

    echo -ne "[$index/$total] $name.$ext->webp\r"
    ffmpeg -v 0 -probesize 42M -i "$img" -vf "scale='min(1366,iw)':-2" -lossless 1 -preset drawing "$output/$name.webp" -y &>/dev/null
done

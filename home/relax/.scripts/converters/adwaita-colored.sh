#!/usr/bin/env bash

# https://medialab.github.io/iwanthue/

typeset -A colors
colors["folder-documents"]="#006363"
colors["folder-download"]="#9f8a00"
colors["folder-music"]="#2bb9d3"
colors["folder-pictures"]="#3a196f"
colors["folder-videos"]="#d3002d"
colors["folder-publicshare"]="#301fc6"
colors["folder-remote"]="#a272ff"
colors["folder-saved-search"]="#ff8522"
colors["folder-templates"]="#00b744"
colors["user-bookmarks"]="#54bc9b"
colors["user-desktop"]="#eb8600"
colors["user-home"]="#af0058"

colors["folder-open"]="copy"
colors["folder-drag-accept"]="copy"
colors["folder"]="copy"

# sizes=("32x32")
sizes=("16x16" "22x22" "24x24" "32x32" "48x48" "512x512")
input_dir="/usr/share/icons/Adwaita"
output_dir="/home/lain/.local/share/icons/Adwaita-Colored"

for size in ${sizes[@]}; do
    mkdir -p "$output_dir/$size/places/"

    for folder in ${!colors[@]}; do
        i=$input_dir/$size/places/$folder.png
        color=${colors[$folder]}

        if [ -f $i ]; then
            o=$output_dir/$size/places/$folder.png

            if [ $color = "copy" ]; then
                echo "Copying $i"
                cp $i $o
                continue
            fi

            echo "Converting $i"

            # grey scale
            convert $i -colorspace gray +repage $o
            # increase contrast, ignore alpha
            convert $o -channel rgb -brightness-contrast 10,5 +channel $o
            # tint
            convert $o -fill $color -colorize 50% $o
        fi
    done
done

gtk-update-icon-cache -f ~/.local/share/icons/Adwaita-Colored

alias sudo="sudo -E"
alias sxiv="sxiv -a"
alias ls="ls --all -l --group-directories-first --color=always --human-readable"
alias pacman="sudo pacman"
alias pac="pacman"
alias pacs="pacman -S"
alias pacr="pacman -Rns"
alias sd="sudo systemctl"
alias sdstart="sd start"
alias sdstatus="sd status"
alias sdstop="sd stop"
alias sdrestart="sd restart"
alias sdu="systemctl --user"
alias mkex="sudo chmod +x"
alias ownme="sudo chown $USER:$USER -R"
alias ownroot="sudo chown root:root -R"
alias cdconfig="cd ~/.config"
alias cdscripts="cd ~/.scripts"
alias cdshare="cd /usr/share"
alias gs='git status'
# alias gc='git commit'
alias gd='git diff'
alias gl='git log'
alias gp='git pull'
alias gr='git reset'
gc() { git clone --depth 1 $1 && cd ${1##*/} }
alias gcf='git clone'

ytv() { youtube-dl "$@" -f "bestvideo[height<=?1080][fps<=?60][vcodec!=?vp9][protocol!=http_dash_segments]+bestaudio/best" }
ytm() { youtube-dl --extract-audio --audio-format mp3 "$@" }
recent() { ls -lt  **/*(.om[1,10]) }
cd() { builtin cd $@ && ls }
preexec() { printf "\033]0;%s\a" "$1" }

unsetopt nomatch
setopt nocaseglob
setopt nocheckjobs
setopt appendhistory
setopt histignorealldups
setopt autocd
setopt prompt_subst

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' rehash true
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh

HISTFILE=~/.cache/zhistory
HISTSIZE=5000
SAVEHIST=5000
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=7'

autoload -U compinit select-word-style colors
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
compinit -d
colors
select-word-style bash
zmodload zsh/terminfo
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"
key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete
[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word
[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search


bindkey '^[[3;2~' kill-word
bindkey '^[[3;3~' kill-word
bindkey '^[[3;5~' kill-word
bindkey '\e^?' backward-kill-word
bindkey '^H' backward-kill-word

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
    autoload -Uz add-zle-hook-widget
    function zle_application_mode_start { echoti smkx }
    function zle_application_mode_stop { echoti rmkx }
    add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
    add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# bindkey -e
# bindkey '^[[7~'             beginning-of-line                               # home
# bindkey '^[[8~'             end-of-line                                     # end
# bindkey '^[[2~'             overwrite-mode                                  # insert
# bindkey '^[[3~'             delete-char                                     # delete
# bindkey '^[[3^'             kill-word                                       # ctrl + delete
# bindkey '^['                kill-word                                       # alt + delete
# bindkey '^H'                backward-kill-word                              # ctrl + backspace
# bindkey '^[[C'              forward-char                                    # right
# bindkey '^[[D'              backward-char                                   # left
# bindkey '^[[5~'             history-beginning-search-backward               # page up
# bindkey '^[[6~'             history-beginning-search-forward                # page down
# bindkey '^[Oc'              forward-word                                    # ctrl + right
# bindkey '^[Od'              backward-word                                   # ctrl + left
# bindkey '^[[1;5D'           backward-word                                   # 
# bindkey '^[[1;5C'           forward-word                                    # 
# bindkey '^[[Z'              undo                                            # shift + tab
# bindkey "$terminfo[kcuu1]"  history-substring-search-up
# bindkey "$terminfo[kcud1]"  history-substring-search-down
# bindkey '^[[A'              history-substring-search-up
# bindkey '^[[B'              history-substring-search-down

# PROMPT="%{$fg[cyan]%}%n%{$fg[white]%}@%{$reset_color%}%{$fg[cyan]%}%M%{$reset_color%} %{$fg[magenta]%}%/
#  %(!.#.λ) %{$reset_color%}"

function branch_name () {
    if [ -d .git ]; then
        branch=$(git symbolic-ref HEAD 2> /dev/null | awk 'BEGIN{FS="/"} {print $NF}')
        echo "git:%{$fg[red]%}$branch%{$reset_color%}"
    fi;
}

PROMPT='%{$fg[cyan]%}%n%{$fg[white]%}@%{$reset_color%}%{$fg[cyan]%}%M%{$reset_color%} %/ $(branch_name) %{$fg[magenta]%}
 %(!.#.λ) %{$reset_color%}'

#!/usr/bin/env bash

for serie in *; do
    serie_dir=$(dirname "$serie")
    index=1

    while read page; do
        extension="${page##*.}"
        mv "$page" "./$serie/$index.${extension}"
        index=$(($index + 1))
    done < <(find "./$serie/" -name "*.jpg" -o -name "*.png" -o -name "*.gif" | sort -V)

done

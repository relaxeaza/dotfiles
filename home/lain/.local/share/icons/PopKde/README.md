# PopKde icon theme

This package is a set of scalable folders based in https://github.com/pop-os/icon-theme, if you prefer you just have to modify the following line in the file *index.theme* to combine with your favorite icon set.

```bash
#Inherits=Papirus,breeze,Yaru,Numix-Circle
Inherits=myFavoriteTheme
```

### Coffee

If you like my work :

Share your ❤️ Buy me a ☕ from [paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=V9Q8MK9CKSQW8&source=url) 
[Liberapay](url=https://liberapay.com/_adhe_/donate)

Have fun ;)
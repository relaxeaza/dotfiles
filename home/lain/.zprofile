export NODE_ENV="development"

export PATH="$HOME/.local/bin:/bin:$PATH"
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export EDITOR="vim"
export VISUAL="subl3"
export RANGER_LOAD_DEFAULT_RC=FALSE
export DROPBOX=$HOME/Dropbox

export GDK_SCALE=1
export GTK_OVERLAY_SCROLLING=0
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi

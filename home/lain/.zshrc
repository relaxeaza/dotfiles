alias sudo="sudo -E"
alias sxiv="sxiv -a"
alias ls="ls --all -l --group-directories-first --color=always --human-readable"
alias pacman="sudo pacman"
alias pac="pacman"
alias pacs="pacman -S"
alias pacr="pacman -Rns"
alias sd="sudo systemctl"
alias mkex="sudo chmod +x"
alias ownme="sudo chown lain:lain -R"
alias ownroot="sudo chown root:root -R"
alias cdconfig="cd ~/.config"
alias cdscripts="cd ~/.scripts"
alias cdshare="cd /usr/share"
alias gs='git status'
alias gc='git commit'
alias gd='git diff'
alias gl='git log'

ytv() { youtube-dl "$@" -f "bestvideo[height<=?1080][fps<=?60][vcodec!=?vp9][protocol!=http_dash_segments]+bestaudio/best" }
ytm() { youtube-dl --extract-audio --audio-format mp3 "$@" }
recent() { ls -lt  **/*(.om[1,10]) }
cd() { builtin cd $@ && ls }
preexec() { printf "\033]0;%s\a" "$1" }

unsetopt nomatch
setopt nocaseglob
setopt nocheckjobs
setopt appendhistory
setopt histignorealldups
setopt autocd

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' rehash true
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh

HISTFILE=~/.cache/zhistory
HISTSIZE=2000
SAVEHIST=500
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=7'

autoload -U compinit select-word-style colors
compinit -d
colors
select-word-style bash
zmodload zsh/terminfo

source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

bindkey -e
bindkey '^[[7~'             beginning-of-line                               # home
bindkey '^[[8~'             end-of-line                                     # end
bindkey '^[[2~'             overwrite-mode                                  # insert
bindkey '^[[3~'             delete-char                                     # delete
bindkey '^[[3^'             kill-word                                       # ctrl + delete
bindkey '^['                kill-word                                       # alt + delete
bindkey '^H'                backward-kill-word                              # ctrl + backspace
bindkey '^[[C'              forward-char                                    # right
bindkey '^[[D'              backward-char                                   # left
bindkey '^[[5~'             history-beginning-search-backward               # page up
bindkey '^[[6~'             history-beginning-search-forward                # page down
bindkey '^[Oc'              forward-word                                    # ctrl + right
bindkey '^[Od'              backward-word                                   # ctrl + left
bindkey '^[[1;5D'           backward-word                                   # 
bindkey '^[[1;5C'           forward-word                                    # 
bindkey '^[[Z'              undo                                            # shift + tab
bindkey "$terminfo[kcuu1]"  history-substring-search-up
bindkey "$terminfo[kcud1]"  history-substring-search-down
bindkey '^[[A'              history-substring-search-up
bindkey '^[[B'              history-substring-search-down

PROMPT="%{$fg[cyan]%}%n%{$fg[white]%}@%{$reset_color%}%{$fg[cyan]%}%M%{$reset_color%} %{$fg[magenta]%}%/
 %(!.#.λ) %{$reset_color%}"

#!/usr/bin/env bash

locate --database ~/.local/share/mlocate.db '' | rofi -columns 1 -dmenu -i -p 'open' | xargs xdg-open

#!/usr/bin/env bash

# defaults
output="$(xdg-user-dir PICTURES)/Screenshots/screenshot-$(date +%Y-%m-%d-%H-%M-%S).png"
temp=/tmp/screenshot.png
supported_engines=('maim' 'scrot')

while [[ "$#" -gt 0 ]]; do case $1 in
    --select) select=1 ;;
    --copy) copy=1 ;;
    --notify) notify=1 ;;
    --show-notif) show_notify=1 ;;
    --open) open_screenshot=1 ;;
    --open-last) open_last_screenshot=1 ;;
    --help) show_help=1 ;;
    --engine) selected_engine=$2; shift ;;
    *) output=$1 ;;
esac; shift; done

if [[ $show_help ]]; then
    echo "$(basename "$0") [-s] [-c] [-n [-d]] [-e] [-o] [OUTPUT]"
    echo "  -s, --select           Interactive selection mode."
    echo "  -c, --copy             Copy the resulting screenshot to the clipboard."
    echo "  -n, --notify           Show a notification with the output location."
    echo "  -d, --show-notif       Include the notification in the screenshot."
    echo "  -o, --open             Open resulting screenshot."
    echo "  -l, --open-last        Open last screenshot taken."
    echo "  -e, --engine=<engine>  Select the screenshot engine. Supported: ${supported_engines[@]}"
    exit 0
fi

if [[ $open_last_screenshot ]]; then
    xdg-open $temp

    exit 0
fi

if [[ $selected_engine ]]; then
    for engine in ${supported_engines[@]}; do
        if [[ $engine == $selected_engine ]]; then
            valid_engine=1

            break
        fi
    done

    if ! [[ $valid_engine ]]; then
        echo "Engine '$selected_engine' not supported." &>2

        exit 1
    fi
else
    for engine in ${supported_engines[@]}; do
        if hash $engine 2>/dev/null; then
            selected_engine="$engine"

            break
        fi
    done

    if ! [[ $selected_engine ]]; then
        echo 'No supported engine available.' &>2

        exit 1
    fi
fi

function screenshot_select () {
    case $selected_engine in
        maim) echo "maim --select $temp --noopengl" ;;
        scrot) echo "scrot --select --overwrite $temp" ;;
    esac
}

function screenshot_instant () {
    case $selected_engine in
        maim) maim $temp --noopengl ;;
        scrot) scrot $temp ;;
    esac
}

output_file=$(basename "$output")
output_dir=${output%%$output_file}

mkdir -p $output_dir

if [[ $select ]]; then
    while $(screenshot_select); do
        if [[ $notify ]]; then
            notify-send 'Screenshot saved' "$output" --app-name screenshot
        fi

        break
    done
else
    if [[ $notify ]]; then
        if [[ $show_notify ]]; then
            notify-send 'Screenshot saved' "$output" --app-name screenshot
            sleep 0.1
            screenshot_instant
        else
            screenshot_instant
            notify-send 'Screenshot saved' "$output" --app-name screenshot
        fi
    else
        screenshot_instant
    fi
fi

if [[ $copy ]]; then
    xclip $temp -selection clipboard -t image/png
fi

cp $temp $output

if [[ $open_screenshot ]]; then
    xdg-open $output
fi

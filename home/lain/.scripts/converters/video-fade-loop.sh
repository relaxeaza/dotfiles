#!/usr/bin/env bash

input=$1
video_duration=$2
fade_duration=$3
output=$4

ffmpeg -i $input -vf "tpad=stop_mode=clone:stop_duration=2" tmp.$input -hide_banner -y
ffmpeg -i tmp.$input -filter_complex "[0]split[body][pre]; [pre]trim=duration=$fade_duration,format=yuva420p,fade=d=$fade_duration:alpha=1,setpts=PTS+($video_duration/TB)[jt]; [body]trim=$fade_duration,setpts=PTS-STARTPTS[main]; [main][jt]overlay" $output -y
rm tmp.$input

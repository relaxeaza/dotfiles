typeset -A colors
colors["folder-documents"]="#0000ff"
colors["folder-download"]="#008000"
colors["folder-music"]="#4b0082"
colors["folder-pictures"]="#ffff00"
colors["folder-publicshare"]="#2f4f4f"
colors["folder-remote"]="#b8860b"
colors["folder-saved-search"]="#db7093"
colors["folder-templates"]="#006e6e"
colors["folder-videos"]="#ff0000"
colors["user-bookmarks"]="#ff00ff"
colors["user-desktop"]="#00ff00"
colors["user-home"]="#6495ed"
colors["folder-open"]="#00ffff"
colors["folder-drag-accept"]="#00ffff"
colors["folder"]="#00ffff"

sizes=("16x16" "22x22" "24x24" "32x32" "48x48" "512x512")
adwaita_dir="/usr/share/icons/Adwaita"
result_dir="/home/lain/.local/share/icons/adwaita-colored"

for size in "${sizes[@]}"; do
    mkdir -p "$result_dir/$size/places/"

    for folder in "${!colors[@]}"; do
        input="$adwaita_dir/$size/places/$folder.png"

        if [ -f "$input" ]; then
            dest="$result_dir/$size/places/$folder.png"

            echo "Converting $input"
            convert "$input" -level "20%,90%,0.5" -fill "${colors[$folder]}" -tint 50 "$dest"
        fi
    done
done

gtk-update-icon-cache -f "~/.local/share/icons/adwaita-colored"

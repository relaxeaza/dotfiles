#!/usr/bin/env bash

# https://medialab.github.io/iwanthue/

typeset -A colors
colors["folder-documents"]="#c899ff"
colors["folder-download"]="#00ba42"
colors["folder-music"]="#962fe1"
colors["folder-pictures"]="#9fa900"
colors["folder-videos"]="#d867ff"
colors["folder-publicshare"]="#c16b00"
colors["folder-remote"]="#003caf"
colors["folder-saved-search"]="#94d8b0"
colors["folder-templates"]="#200059"
colors["user-bookmarks"]="#01a38d"
colors["user-desktop"]="#4f0031"
colors["user-home"]="#adcee3"
colors["folder-open"]="#3f1f00"
colors["folder-drag-accept"]="#e6c1b6"
# colors["folder"]="#001d2a"

sizes=("16x16" "32x32" "48x48")

output_dir="/home/lain/beos-icons-mod"

for size in ${sizes[@]}; do
    for folder in ${!colors[@]}; do
        i="/home/lain/beos-icons-mod/$size/folder-source.png"
        color=${colors[$folder]}
        o=$output_dir/$size/places/$folder.png
        
        echo "Converting $size $folder"

        # grey scale
        convert $i -colorspace gray +repage $o
        # increase contrast, ignore alpha
        convert $o -channel rgb -brightness-contrast 10,5 +channel $o
        # tint
        convert $o -fill $color -colorize 50% $o
    done
done

# gtk-update-icon-cache -f ~/.local/share/icons/Adwaita-Colored

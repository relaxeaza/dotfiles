#!/usr/bin/env bash

output_dir="~/Musics/Converted/"
encode_codec="libmp3lame"
bitrate="128k"
output_ext="mp3"

mkdir -p "$output_dir"
root="$(pwd)"/

for author in */; do
    cd "$root$author"

    for album in */; do
        cd "$root$author$album"
        mkdir -p "$output_dir$author$album"

        echo "creating dir -> \"$output_dir$author$album\""

        for audio in *.{flac,m4a,mp3}; do
            if [ -f "$audio" ]; then
                extension="${audio##*.}"
                filename=$(basename -s ".$extension" "$audio")
                input="$root$author$album$audio"
                output_file=$filename.$output_ext
                output="$output_dir$author$album$output_file"

                if [ -f "$output" ]; then
                    echo "output already exists -> \"$output_file\""
                    continue
                fi

                echo "encoding audio -> \"$audio\" -> \"$output_file\""

                ffmpeg -loglevel error -threads 4 -i "$input" -c:a "$encode_codec" -b:a "$bitrate" "$output" -y
            fi
        done
    done
done

echo "finished"

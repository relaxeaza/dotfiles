#!/usr/bin/env bash

export downscale=1
export downscale_to='640'
export disable_audio=1
export size_limit='2.8M'

if ! [[ -f /tmp/chen-converter.png ]]; then
    base64 --decode > /tmp/chen-converter.png <<< "iVBORw0KGgoAAAANSUhEUgAAAFMAAABkBAMAAAAI4xEmAAAALVBMVEUABABKKjURTEt0MyqXNEbLOTmDWEMagTXkSjXtdjNno1XWsJT/vnn/6b3//9ZbBSfMAAAAAXRSTlMAQObYZgAAAe1JREFUWMPN1r1Lw0AYBvC0DiKC+KLUQRHN0lkPBD9wii7iGHBUlAMX0anQvZDR9QYFdRA6KX4sAcHNobg7dPIDRLi/wefea0yt2slc81CSo/215H3vLqnn/UjRr7Ti+/6U1y3/TllV62059XNI+wSKKFc66Tlqc0XjKzFDlTRVl7ToT1NsQmPWscLUlitkIgQN54SipS2K2hbqqIgB/Ik5jwj8hhN6p/WzEKJ+hgNT/oYpgZK2Zk4/4lspt4WUmOUEXlJuaL+ZyQMSy1prAzupXZv7WdEiVg/vQLGIa5UyoTjwnCKzuD3Y9ZcB5b3PzcJ7c1YC4LireNS2qntNURYnpVummw0TnEvSDZ3mvW7oNtpLE42mCWb1NnvK+IuKeXWs1RHLplL6XeGTHFGkQMmCW2o8ttNnkW7WLKkJ6AzhhaY+KeSoibm9J8eUA9e6dPQpl9S2tyR3gu+bz3NK5Z5+CaIoUgrb0TUt4EJx434NI5NgVRLlmI7HoFwW6FospVOK5f9onrHXAYdGg/RviFN6Y5sV1UJ+vueQFgilrDcbD1EaWx65oYMxNzTc6KQ/m5sNLZA2ncJ0roTW10LAtfi3snpMWU/qjsg/NkI21PMGzJPl0I4x6nZ7y4YOaazmCzvGKOdUv8lNO8bIBf0EE9QHjOa3nywAAAAASUVORK5CYII="
fi

function handler () {
    file=$1
    filename=$(basename -s ".${file##*.}" "$file")
    location=$(dirname "$file")

    if [[ $downscale ]]; then
        downscale_cmd="-vf scale='min($downscale_to,iw)':-2"
    fi

    if [[ $disable_audio ]]; then
        disable_audio_cmd="-an"
    fi

    output="$location/$filename-converted.webm"

    notify-send "Webm Convert" "Started" -i $icon
    ffmpeg -i "$1" $downscale_cmd $disable_audio_cmd -fs $size_limit -c:v libvpx -crf 15 -minrate 500K -maxrate 2M -b:v 500K "$output" -y -v 0 &&
    notify-send "Webm Convert" "Finshed: $location" -i /tmp/chen-converter.png
}

export -f handler

yad --on-top --no-buttons --window-icon /tmp/chen-converter.png --image /tmp/chen-converter.png --title '4chan webm converter' --dnd --exit-on-drop 1 --command="bash -c 'handler \"%s\"'"
